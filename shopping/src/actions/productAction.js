import {
  PRODUCT_DETILS_FAIL,
  PRODUCT_DETILS_REQUEST, PRODUCT_DETILS_SUCCESS,
  PRODUCT_LIST_FAIL,
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS
} from "../contents/procuctsContents";
import axios from "axios";
// 获取所有产品action
export const listProducts = () => async (dispatch) => {
  try{
    dispatch({type: PRODUCT_LIST_REQUEST})
    const { data } = await axios.get('/products')
    dispatch({type: PRODUCT_LIST_SUCCESS, payload: data})
  }catch (error) {
    dispatch({type: PRODUCT_LIST_FAIL, payload:
      error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}
// 获取单个产品action
export const listProductDetails = (id) => async (dispatch) => {
  try{
      dispatch({type: PRODUCT_DETILS_REQUEST})
    const { data } = await axios.get(`/products/${id} `)
    dispatch({type: PRODUCT_DETILS_SUCCESS, payload: data})
  }catch (error) {
    dispatch({type: PRODUCT_DETILS_FAIL, payload:
        error.response && error.response.data.message ? error.response.data.message : error.message,
    })
  }
}