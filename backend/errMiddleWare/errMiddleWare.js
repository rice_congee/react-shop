const notFound = (req, res, next) => {
  const error = new Error(`查询不到${req.originalUrl}`)
  res.status(404)
  next(error);
}

const errHandle = (err, req, res, next) => {
  const statusCode = res.statusCode === 200 ? 500 : res.statusCode;

  res.status(statusCode);
  res.json({
    message: err.message,
    stack: process.env.NODE_ENV === 'production' ? null : err.stack,
  })
}

export { notFound, errHandle };