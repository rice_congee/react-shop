import styled from 'styled-components';
import {Spin} from "antd";

const Loading = styled(Spin)`
  margin: 0 auto;
`
export default Loading;