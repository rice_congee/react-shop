import React from "react";
import {Card, Rate } from 'antd';
// import Rating from './rating';
import CardStyle from './comStyle/produnctstyle';
import { Link } from 'react-router-dom';

const {Meta} = Card;

const Produnct = ({ product }) => {
  return(
    <Link to={`/produnct/${product._id}`}>
      <CardStyle
        hoverable={true}
        cover={<img alt="商品图" src={`${product.img}`} />}
      >
        <Meta
          className="title"
          title={`￥${product.price}`}
          description={`${product.name}`}
        />
        <Rate className="star" allowHalf disabled defaultValue={product.rating} count="5" />
        <span>{`${product.numReviews}条评论`}</span>
      </CardStyle>
    </Link>
    
  )
}
export default Produnct;