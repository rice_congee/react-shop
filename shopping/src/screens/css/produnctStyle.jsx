import styled from 'styled-components';

export const Container = styled.div`
  max-width: 720px;
  margin: 0 auto;

  .shopText {
    .ant-image{
      width: 100%;
    }
  }
`
export const HeaderBox = styled.div`
width: 100%;
height: 44px;
background-color: #0092FF;

.header {
  width: 720px;
  height: 44px;
  margin: 0 auto;
  color: #fff;
  position: fixed;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  
  a {
    color: #fff;
  }
}
  
`

