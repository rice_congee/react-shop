import mongoose from 'mongoose';
// 评论
const reviewSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  rating: {
    type: Number,
    required: true
  },
  comment: {
    type: String,
    required: true
  }
})
// 产品
const productSchema = mongoose.Schema({
  user:{
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  name:{
    type: String,
    required: true
  },
  img:{
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true
  },
  inStock: {
    type: Number,
    required: true
  },
  rating: {
    type: Number,
    required: true
  },
  numReviews: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true,
  },
  reviews: [reviewSchema]
}, {
  timestamps: true
})

const Products = mongoose.model('Products', productSchema)
export default Products;