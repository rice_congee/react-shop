import React from 'react';
import { Carousel  } from 'antd';
import Header from './comStyle/headStyle'


const Head = () => {
  return(
    <Header>
     <Carousel autoplay>
       <div className="banner-img">
        <img src="https://gtms04.alicdn.com/tps/i4/TB1aAIWHpXXXXahXpXXoeuYLVXX-1920-429.jpg" alt=""/>
       </div>
       <div className="banner-img">
        <img src="https://gtms04.alicdn.com/tps/i4/TB1aAIWHpXXXXahXpXXoeuYLVXX-1920-429.jpg" alt=""/>
       </div>
       <div className="banner-img">
        <img src="https://gtms04.alicdn.com/tps/i4/TB1aAIWHpXXXXahXpXXoeuYLVXX-1920-429.jpg" alt=""/>
       </div>
     </Carousel>
    </Header>
  )
}

export default Head;