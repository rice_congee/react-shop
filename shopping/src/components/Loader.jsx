import React from 'react';
import { SyncOutlined  } from '@ant-design/icons';
import Loading from './comStyle/LoadStyle';

const antIcon = <SyncOutlined  />
const Loader = () => {
  return(
    <Loading
      tip={`加载中...`}
      indicator={ antIcon  }
      size="large"
    >
    </Loading>
  )
}

export default Loader;