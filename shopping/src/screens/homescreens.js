import React,{ useEffect } from 'react';
// import axios from 'axios';
import { Row, Col } from 'antd';
import Produnct from '../components/produnct';
import Content from '../components/comStyle/screenStyle'
import Head from '../components/header';
import { useDispatch, useSelector } from "react-redux";
import { listProducts } from "../actions/productAction";
import Loader from "../components/Loader";
import Message from "../components/Message";

const HomeScreen = () => {
  // const [Proudnts, setProdunct] = useState([])
  const dispatch = useDispatch()
  const productList = useSelector(state => state.productList)
  const { loading, error, products } = productList;
  useEffect(() => {
    // const fetchProudnts = async () => {
    //   const { data } = await axios.get('/products');
    //   setProdunct(data);
    // }
    // fetchProudnts();
    dispatch(listProducts())
  }, [dispatch])
    return (
      <Content>
        <Head />
        <h2>最新产品</h2>
        { loading ?
          (<Loader />) : error ?
          (<Message> {error} </Message>) :
          (<Row className="Row">
            {products.map((product) => (
              <Col key={product._id} xs={{ span: 10, offset: 1 }} lg={{ span: 5, offset: 0 }}>
                <Produnct product={product} />
              </Col>
            ))}
          </Row>)
        }
      </Content>
    )
}

export default HomeScreen;