import bcryptjs from 'bcryptjs';
const User = [
  {
    name: 'admin',
    email: 'admin@example.com',
    password: bcryptjs.hashSync('admin', 12),
    isAdmin: true
  }
]

export default User;