import React, { Component } from 'react';
import { BrowserRouter, Switch, Route} from 'react-router-dom'
import 'antd/dist/antd.css';
import ProdunctScreen from './screens/produnctScreen';
import HomeScreen from './screens/homescreens';
export default class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" component={HomeScreen} exact />
          <Route path="/produnct/:_id" component={ProdunctScreen} />
        </Switch>
      </BrowserRouter>
    )
  }
}