import React, { useState, useEffect } from 'react';
// import axios from 'axios';
import {List, Row, Col, Image, Button, Card, Rate, Input} from 'antd';
import { UserOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import {Container, HeaderBox} from './css/produnctStyle';
import {useDispatch, useSelector} from "react-redux";
import {listProductDetails} from "../actions/productAction";
import Loader from "../components/Loader";

//资源图片
// const requireContext = require.context("../assets", true, /^\.\/.*\.jpg$/);
const ProdunctScreen = ({match}) => {
  // const [Proudnts, setProdunct] = useState({});
  const [inStock, setinStock] = useState()
  const dispatch = useDispatch();
  const productDetils = useSelector( state => state.productDetils)
  const {loading, product} = productDetils

  useEffect(() => {
      // const fetchProudnts = async () => {
      //   const { data } = await axios.get(`/products/${match.params._id}`);
      //   setProdunct(data);
      // }
      // fetchProudnts();
    dispatch(listProductDetails(match.params._id))
    }, [dispatch, match])

  return (
    <>
      {
        loading ? (<Loader />) :
        (
          <div>
            <HeaderBox>
              <Row className="header" justify="space-between" align="middle">
                <Col>
                  <Link to="/">回到首页</Link>
                </Col>
                <Col xs={7} sm={4} md={4} lg={4} flex="center">
            <span>
              <UserOutlined />登录
            </span>
                  <span>|</span>
                  <span>
              <ShoppingCartOutlined />购物车
            </span>
                </Col>
              </Row>
            </HeaderBox>
            <Container>
              <Row className="shopText">
                <Col span={10}>
                  <Image src={product.img} preview={false}/>
                </Col>
                <Col span={8} offset={1}>
                  <Card>
                    {product.name}
                  </Card>
                  <Card>
                    <Rate allowHalf disabled value={product.rating} count="5" />
                    <span>{`${product.numReviews}条评论`}</span>
                  </Card>
                  <Card>
                    {product.price}
                  </Card>
                  <Card>
                    {product.description}
                  </Card>
                </Col>
                <Col span={4} offset={1}>
                  <List>
                    <List.Item>
                      {`价格:${product.price}`}
                    </List.Item>
                    <List.Item>
                      {`库存:${product.inStock > 0 ? product.inStock : '没货了'}`}
                    </List.Item>
                    <List.Item>
                      <Input.Group>
                        <Input.Search></Input.Search>
                      </Input.Group>
                    </List.Item>
                    <List.Item>
                      <Button type="primary" {...product.inStock > 0 ? '' : {disabled:true}}>添加到购物车</Button>
                    </List.Item>
                  </List>
                </Col>
              </Row>
            </Container>
          </div>
        )
      }
    </>
  )
}
export default ProdunctScreen;