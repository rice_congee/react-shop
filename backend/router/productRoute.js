import express from 'express';
import asyncHandler from 'express-async-handler';
import Products from '../models/productModel.js';

const router = express.Router();
//@desc 请求所有产品
//route GET/products
//@access 公开路由
router.get('/', asyncHandler(async (req, res)=>{

  const products = await Products.find({});

  res.json(products)
}))

//@desc 请求某个产品
//route GET/products/:id
//@access 公开路由
router.get('/:id', asyncHandler(async (req, res)=>{
  const product = await Products.findById(req.params.id);
  console.log(req);
  if(product) {
    res.json(product)
  }else {
    res.status(404)
    throw new Error(`查询不到这个产品`);
  }
}))

export default router;