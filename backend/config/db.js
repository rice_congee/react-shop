import mongoose from 'mongoose';
const connectDB = async () => {
  try {
    const coon = await mongoose.connect(process.env.MONGODB, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true
    });
    console.log(`MONGODB 已连接 ${coon.connection.host}`)
  } catch (err) {
    console.log(`ERROR:${err.message}`)
    process.exit(1)
  }
}

export default connectDB;