import styled from 'styled-components';

const Header = styled.div`
  width: 100%;
  margin: 0 auto;
  background: #000;
  color: #fff;

  .banner-img {
    width: 100%;

    img {
      width: 100%;
      height: auto;
    }
  }
`
export default Header;