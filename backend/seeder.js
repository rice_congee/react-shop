import monogoose from 'mongoose';
import dotenv from 'dotenv';
import user from './data/user.js';
import proud from './data/proud.js';
import usermodel from './models/usermodel.js';
import productModel from './models/productModel.js';
import orderModel from './models/orderModel.js';
import connectDB from './config/db.js';

dotenv.config();
connectDB();

const importData = async () => {
  try {
    //清空数据库的样本数据
    await orderModel.deleteMany();
    await usermodel.deleteMany();
    await productModel.deleteMany();

    //样本数据插入
    const CreatedUser = await usermodel.insertMany(user);
    const adminUser = CreatedUser[0]._id;
    const sampleProduct = await proud.map((Proudnts) => {
      return {...Proudnts, user: adminUser}
    });

    await productModel.insertMany(sampleProduct);
    console.log(`样本数据插入成功`);
    process.exit();
  } catch (error) {
    console.error(error);
  }
}

const destoryData = async () => {
  try {
    //清空数据库的样本数据
    await orderModel.deleteMany();
    await usermodel.deleteMany();
    await productModel.deleteMany();

    console.log(`样本数据销毁成功`);
    process.exit();
  } catch (error) {
    console.error(error);
  }
}
if(process.argv[2] === 'd') {
  destoryData();
}else {
  importData();
}
