import styled from 'styled-components';

const Content = styled.div`
  max-width: 960px;
  margin: 0 auto;

  h2 {
    margin: 0;
  }

  .Row {
    display: flex;
    justify-content: space-between
  }
`
export default Content;