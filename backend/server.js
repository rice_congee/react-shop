import express from 'express';
import { notFound, errHandle } from './errMiddleWare/errMiddleWare.js'
import dotenv from 'dotenv';
import connectDB from './config/db.js';
import router from './router/productRoute.js';
dotenv.config();
connectDB();
const app = express();

app.get('/', (req, res)=>{
  res.send("server is running")
})

app.use('/products', router);

// 错误界面中间件
app.use(notFound);
app.use(errHandle);

const PROT = process.env.PORT || 5000;

app.listen(PROT, console.log(`服务器在${process.env.NODE_ENV}模式下运行，端口号为:${PROT}`));