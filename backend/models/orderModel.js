import mongoose from 'mongoose';

const oderSchema = mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  oderItem:[
    {
      name: {
        type: String,
        required: true
      },
      sum: {
        type: Number,
        required: true
      },
      img:{
        type: String,
        required: true,
      },
      price: {
        type: Number,
        required: true
      },
      product: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Products'
      }
    }
  ],
  email:{
    type: String,
    required: true,
    unique: true
  },
  shippingAddress: {
    Address: {
      type: String,
      required: true
    },
    province: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    postalCode: {
      type: Number,
      required: true
    }
  },
  pay: {
    type: String,
    required: true
  },
  payResult: {
    id: {
      type: String
    },
    status: {
      type: String
    },
    updateTime: {
      type: String
    },
    isDelivered: {
      type: Boolean,
      required: true,
      default: false
    },
    Delivered: {
      type: Date
    },
    isPay: {
      type: Boolean,
      default: false
    }
  }
}, {
  timestamps: true
})

const Oder = mongoose.model('Oder', oderSchema)
export default Oder