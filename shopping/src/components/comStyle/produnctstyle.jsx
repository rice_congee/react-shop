import {Card} from 'antd';
import styled from 'styled-components';

const CardStyle = styled(Card)`
  margin-top: 20px;
  width: 220px;

  .ant-card-body {
    padding: 10px 15px;

    .ant-card-meta-title {
      color: #f04134;
      font-size: 20px;
      margin-bottom: 0;
    }
    .star {
      li {
        margin-right: 4px;
      }
    }
  }

  .title {
    font-size: 16px;
  }
`

export default CardStyle;