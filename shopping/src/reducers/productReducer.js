import {
  PRODUCT_LIST_REQUEST,
  PRODUCT_LIST_SUCCESS,
  PRODUCT_LIST_FAIL,
  PRODUCT_DETILS_REQUEST, PRODUCT_DETILS_SUCCESS, PRODUCT_DETILS_FAIL
} from '../contents/procuctsContents'
// 获取所有产品reducer
export const productReducer = (state = {products: []}, action) => {
  switch (action.type) {
    case PRODUCT_LIST_REQUEST:
      return {loading: true, products: []};
    case PRODUCT_LIST_SUCCESS:
      return {loading: false,products: action.payload}
    case PRODUCT_LIST_FAIL:
      return {loading: false,err: action.payload}
    default:
      return state
  }
}

// 获取单个产品reducer
export const productDetilsReducer = (state = {product: {}}, action) => {
  switch (action.type) {
    case PRODUCT_DETILS_REQUEST:
      return {loading: true, ...state};
    case PRODUCT_DETILS_SUCCESS:
      return {loading: false,product: action.payload}
    case PRODUCT_DETILS_FAIL:
      return {loading: false,err: action.payload}
    default:
      return state
  }
}