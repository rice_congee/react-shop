import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import {productDetilsReducer, productReducer} from './reducers/productReducer';

const reducer = combineReducers({
  productList: productReducer,
  productDetils: productDetilsReducer
})
const initialState = {}
const middleware = [thunk]

const store = createStore(reducer, initialState, composeWithDevTools(
  applyMiddleware(...middleware),
  // other store enhancers if any
));

export default store;